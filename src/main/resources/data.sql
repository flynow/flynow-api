insert into address(street,number,complement,district,city,state,country,zip_code) values('rua vinte e quatro de outubro',2284,'sem complento','fatima','santarem','para','brasil',68040010);
insert into company(name,address_id) values ('Taxi Aereo Santarem',1);
insert into address(street,number,complement,district,city,state,country,zip_code) values('travessa frei ambrosio',925,'casa b','fatima','santarem','para','brasil',68040440);
insert into company(name,address_id) values ('Taxi Aereo Voe Agora',2);

insert into aircraft(model,company_id) values ('CARAVAN C208',1);
insert into image_aircraft(aircraft_id,url)values(1,'https://fly-now-test.s3-sa-east-1.amazonaws.com/C208_1.jpg');
insert into image_aircraft(aircraft_id,url)values(1,'https://fly-now-test.s3-sa-east-1.amazonaws.com/C208_2.JPG');
insert into aircraft(model,company_id) values ('CESSNA 210',2);
insert into image_aircraft(aircraft_id,url)values(2,'https://fly-now-test.s3-sa-east-1.amazonaws.com/C210_1.jpg');
insert into image_aircraft(aircraft_id,url)values(2,'https://fly-now-test.s3-sa-east-1.amazonaws.com/C210_2.jpg');


insert into air_service (name,description,origin,destination, repeat,end_date,days_cycle,company_id,aircraft_id,type) values('Viagem Santarem Belem','description','SBSN','SBBE',true,'2020-12-31 23:59:00.000',7,1,1,0);
insert into travel(ticket_price,departure,min_capacity,max_capacity,current_capacity,id) values(100.0,'2020-12-01 10:00:00.000',5,30,0,1);
insert into image_service(air_service_id,url)values(1,'https://fly-now-test.s3-sa-east-1.amazonaws.com/belem-01.jpg');


insert into air_service (name,description,origin,destination, repeat,end_date,days_cycle,company_id,aircraft_id,type) values('Santarem - Belem','description','SBSN','SBBE',true,'2020-12-31 23:59:00.000',7,1,1,1);
insert into cargo(unit_price,max_weight,max_volume,max_height,max_width,max_length,id) values (10.00,1000.0,50.0,2.5,5.0,4.0,2);
insert into image_service(air_service_id,url)values(2,'https://fly-now-test.s3-sa-east-1.amazonaws.com/belem-02.jpg');

insert into air_service (name,description,origin,destination, repeat,end_date,days_cycle,company_id,aircraft_id,type) values('Frete de Aviao','description','SBSN','SBBE',true,'2020-12-31 23:59:00.000',7,2,2,2);
insert into freight(hour_price,max_passengers,id) values (2000.00,30,3);
insert into image_service(air_service_id,url)values(3,'https://fly-now-test.s3-sa-east-1.amazonaws.com/belem-03.jpg');
