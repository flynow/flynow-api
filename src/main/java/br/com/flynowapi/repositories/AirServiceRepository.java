package br.com.flynowapi.repositories;

import br.com.flynowapi.model.AirService;
import br.com.flynowapi.model.Aircraft;
import br.com.flynowapi.model.Company;
import br.com.flynowapi.model.enums.ServiceType;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface AirServiceRepository extends PagingAndSortingRepository<AirService,Long> {

    public List<AirService> findAllByAircraft(Aircraft aircraft);
    public List<AirService> findAllByCompany(Company company);
    public Optional<List<AirService>> findAllByType(ServiceType type);
}
