package br.com.flynowapi.repositories;

import br.com.flynowapi.model.Aircraft;
import br.com.flynowapi.model.Company;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AircraftRepository extends PagingAndSortingRepository<Aircraft, Long> {


    public List<Aircraft> findAllByCompany(Company company);

}
