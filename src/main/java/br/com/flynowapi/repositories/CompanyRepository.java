package br.com.flynowapi.repositories;

import br.com.flynowapi.model.Company;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CompanyRepository extends PagingAndSortingRepository<Company,Long> {
}
