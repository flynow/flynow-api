package br.com.flynowapi.resources;

import br.com.flynowapi.model.AirService;
import br.com.flynowapi.model.dto.*;
import br.com.flynowapi.services.AirServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "services")
public class AirServiceResource {

    @Autowired
    private AirServiceService airServiceService;

    @GetMapping
    public ResponseEntity<List<AirServiceSimpleDTO>> findAll() {
        List<AirServiceSimpleDTO> dtoList = new ArrayList<>();
        airServiceService.findAll().forEach(s -> {
                    var dto = new AirServiceSimpleDTO(s.getId(), s.getName(), s.getDescription(), s.getType().getDescription());
                    dtoList.add(dto);
                    s.getImages().forEach(l -> dto.add(Link.of(l, "images")));
                }
        );
        return ResponseEntity.ok().body(dtoList);
    }


    @GetMapping(value = "/type/{description}")
    public ResponseEntity<List<AirServiceSimpleDTO>> findByType(@PathVariable String description) {
        List<AirServiceSimpleDTO> dtoList = new ArrayList<>();
        airServiceService.findByType(description).forEach(s -> {
                    var dto = new AirServiceSimpleDTO(s.getId(), s.getName(), s.getDescription(), s.getType().getDescription());
                    dtoList.add(dto);
                    s.getImages().forEach(l -> dto.add(Link.of(l, "images")));
                }
        );
        return ResponseEntity.ok().body(dtoList);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AirServiceDTO> findById(@PathVariable long id) {
        AirService airService = airServiceService.findById(id);
        AirServiceDTO dto = AirServiceDTO.of(airService);
        Link linkCompany = linkTo(methodOn(CompanyResource.class)
                .findById(airService.getCompany().getId())
        ).withRel("company");
        dto.add(linkCompany);
        Link linkAircraft = linkTo(methodOn(AircraftResource.class)
                .findById(airService.getAircraft().getId())
        ).withRel("aircraft");
        dto.add(linkAircraft);
        return ResponseEntity.ok().body(dto);
    }


}
