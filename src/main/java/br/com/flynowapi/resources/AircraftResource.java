package br.com.flynowapi.resources;

import br.com.flynowapi.model.Aircraft;
import br.com.flynowapi.model.dto.AircraftDTO;
import br.com.flynowapi.model.dto.AircraftSimpleDTO;
import br.com.flynowapi.services.AircraftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/aircraft")
public class
AircraftResource {

    @Autowired
    private AircraftService aircraftService;

    @GetMapping
    public ResponseEntity<List<AircraftSimpleDTO>> findAll() {
        List<AircraftSimpleDTO> dtoList = new ArrayList<>();
        aircraftService.findAll().forEach(a -> {
            AircraftSimpleDTO dto = new AircraftSimpleDTO(a.getId(), a.getModel());
            a.getImages().forEach(i -> dto.add(Link.of(i, "images")));
            dtoList.add(dto);
        });
        return ResponseEntity.ok().body(dtoList);
    }

    @GetMapping(value = "/company/{company_id}")
    public ResponseEntity<List<AircraftSimpleDTO>> findAllByCompany(@PathVariable Long company_id) {
        List<AircraftSimpleDTO> dtoList = new ArrayList<>();
        aircraftService.findAllByCompany(company_id).forEach(a -> {
            AircraftSimpleDTO dto = new AircraftSimpleDTO(a.getId(), a.getModel());
            a.getImages().forEach(i -> dto.add(Link.of(i, "images")));
            dtoList.add(dto);
        });
        return ResponseEntity.ok().body(dtoList);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AircraftDTO> findById(@PathVariable Long id) {
        Aircraft aircraft = aircraftService.findById(id);
        AircraftDTO dto = new AircraftDTO(aircraft);
        Link link = linkTo(methodOn(CompanyResource.class)
                .findById(aircraft.getCompany().getId())
        ).withRel("company");
        aircraft.getImages().forEach(l -> dto.add(Link.of(l, "images")));
        dto.add(link);
        return ResponseEntity.ok().body(dto);
    }

}
