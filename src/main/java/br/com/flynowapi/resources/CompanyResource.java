package br.com.flynowapi.resources;

import br.com.flynowapi.model.AirService;
import br.com.flynowapi.model.Aircraft;
import br.com.flynowapi.model.Company;
import br.com.flynowapi.model.dto.CompanyDTO;
import br.com.flynowapi.model.dto.CompanySimpleDTO;
import br.com.flynowapi.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping(value = "/companies")
public class CompanyResource {

    @Autowired
    CompanyService companyService;

    @GetMapping
    public ResponseEntity<List<CompanySimpleDTO>> findAll() {
        List<CompanySimpleDTO> dtoList = new ArrayList<>();
        companyService.findAll().forEach(c -> dtoList.add(new CompanySimpleDTO(c.getId(), c.getName())));
        return ResponseEntity.ok().body(dtoList);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CompanyDTO> findById(@PathVariable Long id) {
        Company company = companyService.findById(id);
        CompanyDTO dto = new CompanyDTO(company);
        for (Aircraft a : company.getAircraft()) {
            Link link = linkTo(methodOn(AircraftResource.class)
                    .findById(a.getId())
            ).withRel("aircraft");
            dto.add(link);
        }
        for (AirService s : company.getAirServices()) {
            Link link = linkTo(methodOn(AirServiceResource.class)
                    .findById(s.getId())
            ).withRel("services");
            dto.add(link);
        }

        return ResponseEntity.ok().body(dto);
    }

}
