package br.com.flynowapi.resources.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

@ControllerAdvice
public class ResourceExceptionHandler implements Serializable {


    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<StandardError> objectNotFoundException(EntityNotFoundException ex, HttpServletRequest request){
        StandardError error = new StandardError(System.currentTimeMillis(),HttpStatus.NOT_FOUND.value(),ex.getMessage(),"",request.getRequestURI());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<StandardError> IllegalArgumentException(IllegalArgumentException ex, HttpServletRequest request){
        StandardError error = new StandardError(System.currentTimeMillis(),HttpStatus.BAD_REQUEST.value(),ex.getMessage(),"",request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }


}
