package br.com.flynowapi.model;

import br.com.flynowapi.model.enums.ServiceType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Travel extends AirService implements Serializable {

    private Double ticketPrice;
    private Date departure;
    private Integer minCapacity;
    private Integer maxCapacity;
    private Integer currentCapacity;

    public Travel(long id, String name, String description, String origin, String destination,
                  Boolean repeat, Date endDate, Integer daysCycle,
                  @NotNull(message = "Field 'company' is required") Company company,
                  Aircraft aircraft, Double ticketPrice, Date departure, Integer minCapacity,
                  Integer maxCapacity, Integer currentCapacity) {

        super(id, name, description, origin, destination, repeat, endDate, daysCycle,
                ServiceType.TRAVEL, company, aircraft);

        this.ticketPrice = ticketPrice;
        this.departure = departure;
        this.minCapacity = minCapacity;
        this.maxCapacity = maxCapacity;
        this.currentCapacity = currentCapacity;
    }
}
