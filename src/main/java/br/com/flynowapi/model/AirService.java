package br.com.flynowapi.model;

import br.com.flynowapi.model.enums.ServiceType;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Inheritance(strategy = InheritanceType.JOINED)
@Entity
public abstract class AirService implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private long id;

    private String name;
    private String description;
    private String origin;
    private String destination;
    private Timestamp instant;
    private Boolean repeat;
    private Date endDate;
    private Integer daysCycle;
    private ServiceType type;

    @ElementCollection
    @CollectionTable(name = "image_service")
    @Column(name = "url")
    private List<String> images;

    @ManyToOne
    @JoinColumn(name = "company_id")
    @NotNull(message = "Field 'company' is required")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "aircraft_id")
    private Aircraft aircraft;


    public AirService(long id, String name, String description, String origin, String destination,
                      Boolean repeat, Date endDate, Integer daysCycle, ServiceType type,
                      @NotNull(message = "Field 'company' is required") Company company, Aircraft aircraft) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.origin = origin;
        this.destination = destination;
        this.repeat = repeat;
        this.endDate = endDate;
        this.daysCycle = daysCycle;
        this.type = type;
        this.company = company;
        this.aircraft = aircraft;
        this.images = new ArrayList<>();
    }

    public Boolean addImage(String url) {
        return images.add(url);
    }
}
