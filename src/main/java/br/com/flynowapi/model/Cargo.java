package br.com.flynowapi.model;

import br.com.flynowapi.model.enums.ServiceType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
public class Cargo extends AirService implements Serializable {

    private Double unitPrice;
    private Double maxWeight;
    private Double maxVolume;
    private Double maxHeight;
    private Double maxWidth;
    private Double maxLength;

    public Cargo(long id, String name, String description, String origin, String destination,
                 Boolean repeat, Date endDate, Integer daysCycle,
                 @NotNull(message = "Field 'company' is required") Company company,
                 Aircraft aircraft, Double unitPrice, Double maxWeight,
                 Double maxVolume, Double maxHeight, Double maxWidth, Double maxLength) {

        super(id, name, description, origin, destination, repeat, endDate, daysCycle, ServiceType.CARGO, company, aircraft);
        this.unitPrice = unitPrice;
        this.maxWeight = maxWeight;
        this.maxVolume = maxVolume;
        this.maxHeight = maxHeight;
        this.maxWidth = maxWidth;
        this.maxLength = maxLength;
    }
}
