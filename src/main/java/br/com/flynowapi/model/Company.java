package br.com.flynowapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Company implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private long id;

    @NotNull(message = "the field 'name' is required")
    @Column(nullable = false)
    private String name;

    private String tradeName;
    private Integer registeredNumber;
    private Integer airRegisteredNumber;

    @OneToOne
    @JoinColumn(name = "address_id")
    private Address address;

    @JsonIgnore
    @OneToMany(mappedBy = "company")
    List<Aircraft> aircraft;
    @JsonIgnore
    @OneToMany(mappedBy = "company")
    List<AirService> airServices;

    public Company(long id, @NotNull(message = "the field 'name' is required") String name, Address address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
}
