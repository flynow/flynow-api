package br.com.flynowapi.model;

import br.com.flynowapi.model.enums.ServiceType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Freight extends AirService implements Serializable {

    private double hourPrice;
    private double maxPassengers;

    public Freight(long id, String name, String description, String origin, String destination,
                   Boolean repeat, Date endDate, Integer daysCycle,
                   @NotNull(message = "Field 'company' is required") Company company,
                   Aircraft aircraft, double hourPrice, double maxPassengers) {
        super(id, name, description, origin, destination, repeat,
                 endDate, daysCycle, ServiceType.FREIGHT, company, aircraft);
        this.hourPrice = hourPrice;
        this.maxPassengers = maxPassengers;
    }
}
