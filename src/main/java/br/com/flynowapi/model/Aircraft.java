package br.com.flynowapi.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Aircraft {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private long id;

    @NotNull(message = "the field 'model' is required")
    @Column
    private String model;

    private String manufacturer;
    private Double maxSpeed;
    private Double cruiseSpeed;
    private Integer capacity;
    private Double maxWeight;
    @Column(name = "max_range")
    private Integer maxRange;


    @ElementCollection
    @CollectionTable(name = "image_aircraft")
    @Column(name = "url")
    private List<String> images;

    @ManyToOne
    @JoinColumn(name = "company_id")
    public Company company;

    public Aircraft(long id, @NotNull(message = "the field 'model' is required") String model, Company company) {
        this.id = id;
        this.model = model;
        this.company = company;
        images = new ArrayList<>();
    }

    public void addImage(String uri) {
        if (images == null)
            images = new ArrayList<>();
        images.add(uri);
    }
}
