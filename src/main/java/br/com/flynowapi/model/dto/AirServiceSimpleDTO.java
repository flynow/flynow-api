package br.com.flynowapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AirServiceSimpleDTO extends RepresentationModel<AirServiceSimpleDTO> implements Serializable {

    private long id;
    private String name;
    private String description;
    private String type;
}
