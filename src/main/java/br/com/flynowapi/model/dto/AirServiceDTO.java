package br.com.flynowapi.model.dto;

import br.com.flynowapi.model.AirService;
import br.com.flynowapi.model.Cargo;
import br.com.flynowapi.model.Freight;
import br.com.flynowapi.model.Travel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public abstract class AirServiceDTO extends RepresentationModel<AirServiceDTO> {

    private long id;
    private String name;
    private String description;
    private String type;

    public AirServiceDTO(AirService airService) {
        this.id = airService.getId();
        this.name = airService.getName();
        this.description = airService.getDescription();
        this.type = airService.getType().getDescription();
    }

    public static AirServiceDTO of(AirService airService) {
        AirServiceDTO dto;
        if (airService instanceof Cargo)
            dto = new CargoDTO((Cargo) airService);
        else if (airService instanceof Freight)
            dto = new FreightDTO((Freight) airService);
        else
            dto = new TravelDTO((Travel) airService);
        return dto;
    }
}
