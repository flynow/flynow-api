package br.com.flynowapi.model.dto;

import br.com.flynowapi.model.Address;
import br.com.flynowapi.model.Company;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CompanyDTO extends RepresentationModel<CompanyDTO> {

    private long id;
    private String name;
    private String tradeName;
    private Integer registeredNumber;
    private Integer airRegisteredNumber;
    private Address address;


    public CompanyDTO(Company company) {
        this.id = company.getId();
        this.name = company.getName();
        this.tradeName = company.getTradeName();
        this.registeredNumber = company.getRegisteredNumber();
        this.airRegisteredNumber = company.getAirRegisteredNumber();
        this.address = company.getAddress();
    }
}
