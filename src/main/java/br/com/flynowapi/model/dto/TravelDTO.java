package br.com.flynowapi.model.dto;

import br.com.flynowapi.model.Travel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TravelDTO extends AirServiceDTO{

    private String origin;
    private String destination;

    public TravelDTO(Travel travel) {
        super(travel);
        this.origin = travel.getOrigin();
        this.destination = travel.getDestination();
    }
}
