package br.com.flynowapi.model.dto;

import br.com.flynowapi.model.Freight;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FreightDTO extends AirServiceDTO {

    private double hourPrice;

    public FreightDTO(Freight freight) {
        super(freight);
        this.hourPrice = freight.getHourPrice();
    }
}
