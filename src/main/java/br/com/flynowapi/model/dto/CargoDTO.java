package br.com.flynowapi.model.dto;

import br.com.flynowapi.model.Cargo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CargoDTO extends AirServiceDTO {
    private double unitPrice;
    public CargoDTO(Cargo cargo) {
        super(cargo);
        this.unitPrice = cargo.getUnitPrice();
    }
}
