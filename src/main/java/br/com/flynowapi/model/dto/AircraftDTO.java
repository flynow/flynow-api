package br.com.flynowapi.model.dto;

import br.com.flynowapi.model.Aircraft;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AircraftDTO  extends RepresentationModel<AircraftDTO> {

    private long id;

    @NotNull(message = "the field 'model' is required")
    private String model;

    private String manufacturer;
    private Double maxSpeed;
    private Double cruiseSpeed;
    private Integer capacity;
    private Double maxWeight;
    private Integer maxRange;

    public AircraftDTO(Aircraft aircraft) {
        this.model = aircraft.getModel();
        this.manufacturer = aircraft.getManufacturer();
        this.maxSpeed = aircraft.getMaxSpeed();
        this.cruiseSpeed = aircraft.getCruiseSpeed();
        this.capacity = aircraft.getCapacity();
        this.maxWeight = aircraft.getMaxWeight();
        this.maxRange = aircraft.getMaxRange();
    }
}
