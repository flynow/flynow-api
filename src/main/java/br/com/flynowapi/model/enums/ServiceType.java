package br.com.flynowapi.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ServiceType {
    TRAVEL((byte) 0,"travel"),
    CARGO((byte) 1,"cargo"),
    FREIGHT((byte) 2,"freight");


    private final byte code;
    private final String description;

    public static ServiceType toEnum(byte code) {

        for (ServiceType type : ServiceType.values()) {
            if (code == type.code)
                return type;
        }
        throw new IllegalArgumentException("Invalid code <" + code + ">");
    }

    public static ServiceType toEnum(String description) {

        for (ServiceType type : ServiceType.values()) {
            if (description.equalsIgnoreCase(type.description))
                return type;
        }
        throw new IllegalArgumentException("Invalid description <" + description + ">");
    }
}
