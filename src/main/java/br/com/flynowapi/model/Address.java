package br.com.flynowapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String street;
    private Integer number;
    private String complement;
    private String district;
    private String city;
    private String state;
    private String country;
    private Integer zipCode;

    @JsonIgnore
    @OneToOne(mappedBy = "address")
    private Company company;

    public Address(long id, String street, Integer number, String complement, String district, String city, String state, String country, Integer zipCode) {
        this.id = id;
        this.street = street;
        this.number = number;
        this.complement = complement;
        this.district = district;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zipCode = zipCode;
    }
}
