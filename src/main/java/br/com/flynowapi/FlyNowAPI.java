package br.com.flynowapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlyNowAPI {

    public static void main(String[] args) {
        SpringApplication.run(FlyNowAPI.class, args);
    }

}
