package br.com.flynowapi.services;

import br.com.flynowapi.model.Aircraft;
import br.com.flynowapi.model.Company;
import br.com.flynowapi.repositories.AircraftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AircraftService {

    @Autowired
    AircraftRepository aircraftRepository;

    public List<Aircraft> findAll() {
        List<Aircraft> aircraftList = new ArrayList<>();
        aircraftRepository.findAll().forEach(a -> aircraftList.add(a));
        return aircraftList;
    }

    public List<Aircraft> findAllByCompany(long company_id) {
        return aircraftRepository.findAllByCompany(new Company(company_id,null,null));
    }

    public Aircraft findById(Long id) {
        return aircraftRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Entity not found! Id:" + id + ", Class:" + Aircraft.class.getSimpleName()+"."));
    }
}
