package br.com.flynowapi.services;

import br.com.flynowapi.model.AirService;
import br.com.flynowapi.model.enums.ServiceType;
import br.com.flynowapi.repositories.AirServiceRepository;
import com.amazonaws.services.devicefarm.model.ArgumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AirServiceService {

    @Autowired
    private AirServiceRepository airServiceRepository;

    public List<AirService> findAll() {
        List<AirService> airServices = new ArrayList<>();
        airServiceRepository.findAll().forEach(as -> airServices.add(as));
        return airServices;
    }

    public AirService findById(long id) {
        return airServiceRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Entity not found! Id:" + id + ", Class:" + AirService.class.getSimpleName() + "."));
    }

    public List<AirService> findByType(String description) {
        return airServiceRepository.findAllByType(ServiceType.toEnum(description)).orElseThrow(() -> new IllegalArgumentException("Invalid description <" + description + ">"));
    }
}
