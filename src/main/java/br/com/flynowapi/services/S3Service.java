package br.com.flynowapi.services;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

@Service
public class S3Service {

    private final Logger LOG = LoggerFactory.getLogger(S3Service.class);


    @Autowired
    private AmazonS3 s3Client;

    @Value("${s3.bucket}")
    private String bucketName;

    public URI uploadFile(String localFilePath, String fileName) throws URISyntaxException {

        File file = new File(localFilePath);
        LOG.info("Starting upload...");
        s3Client.putObject(new PutObjectRequest(bucketName, fileName, file));

        URI uri = s3Client.getUrl(bucketName, fileName).toURI();
        LOG.info("Upload finished.");

        return uri;
    }

    public URI uploadFile(InputStream is, String fileName, String contentType) throws URISyntaxException {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(contentType);
        s3Client.putObject(bucketName, fileName, is, metadata);
        URI uri = s3Client.getUrl(bucketName, fileName).toURI();
        return uri;
    }

    public URI uploadFile(MultipartFile multipartFile) throws IOException, URISyntaxException {
        return uploadFile(multipartFile.getInputStream(), multipartFile.getOriginalFilename(), multipartFile.getContentType());
    }

    public boolean deleteFile(String fileName) {
        s3Client.deleteObject(bucketName, fileName);
        return !s3Client.doesObjectExist(bucketName, fileName);
    }
}
