package br.com.flynowapi.services;

import br.com.flynowapi.model.Company;
import br.com.flynowapi.repositories.CompanyRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public Company findById(Long id) {
        return companyRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Entity not found! Id:" + id + ", Class:" + Company.class.getSimpleName()+"."));
    }

    public List<Company> findAll() {
        List<Company> list = new ArrayList<>();
        companyRepository.findAll().forEach(c -> list.add(c));
        return list;
    }

}
