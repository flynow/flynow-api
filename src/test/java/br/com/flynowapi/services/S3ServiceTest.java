package br.com.flynowapi.services;

import br.com.flynowapi.ApplicationTest;
import com.amazonaws.SdkClientException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

public class S3ServiceTest extends ApplicationTest {

    @Autowired
    private S3Service s3Service;

    private String fileName = "cessna.jpg";

    @Test
    public void saveLocalImage() throws URISyntaxException {
       URI uri = null;
       try {
           uri = s3Service.uploadFile("src/test/resources/img/"+ fileName, fileName);
       }catch (SdkClientException ex){

       }
       assertNotNull(uri);
    }

    @Test
    public void deleteS3Image(){
        assertTrue(s3Service.deleteFile(fileName));
    }


}
