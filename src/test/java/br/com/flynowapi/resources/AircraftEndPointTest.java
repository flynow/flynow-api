package br.com.flynowapi.resources;

import br.com.flynowapi.ApplicationTest;
import br.com.flynowapi.model.Address;
import br.com.flynowapi.model.Aircraft;
import br.com.flynowapi.model.Company;
import br.com.flynowapi.model.dto.AircraftDTO;
import br.com.flynowapi.model.dto.AircraftSimpleDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.http.ContentType;
import org.junit.Test;
import org.springframework.hateoas.Link;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class AircraftEndPointTest extends ApplicationTest {


    @Test
    public void getListAircraft() throws JsonProcessingException {
        AircraftSimpleDTO dto1 = new AircraftSimpleDTO(aircraft1.getId(), aircraft1.getModel());
        aircraft1.getImages().forEach(i -> dto1.add(Link.of(i, "images")));

        AircraftSimpleDTO dto2 = new AircraftSimpleDTO(aircraft2.getId(), aircraft2.getModel());
        aircraft2.getImages().forEach(i -> dto2.add(Link.of(i, "images")));

        List<AircraftSimpleDTO> aircraftList = Arrays.asList(dto1, dto2);

        String jsonExpected = objectMapper.writeValueAsString(aircraftList);

        String jsonActual = given().when()
                .get("/aircraft")
                .asString();

        assertEquals(jsonExpected, jsonActual);
    }

    @Test
    public void getListAircraftByCompany() throws JsonProcessingException {
        AircraftSimpleDTO dto1 = new AircraftSimpleDTO(aircraft1.getId(), aircraft1.getModel());
        aircraft1.getImages().forEach(i -> dto1.add(Link.of(i, "images")));
        List<AircraftSimpleDTO> aircraftList = new ArrayList<>();
        aircraftList.add(dto1);

        String jsonExpected = objectMapper.writeValueAsString(aircraftList);

        String jsonActual = given().when()
                .get("/aircraft/company/1")
                .asString();

        assertEquals(jsonExpected, jsonActual);


    }

    @Test
    public void getAircraftById() throws JsonProcessingException {
//        AircraftDTO dto = new AircraftDTO(aircraft1);
//        Link link = linkTo(methodOn(CompanyResource.class)
//                .findById(aircraft1.getCompany().getId())
//        ).withRel("company");
//        aircraft1.getImages().forEach(l -> dto.add(Link.of(l, "images")));
//        dto.add(link);
//        String jsonExpected = objectMapper.writeValueAsString(dto);

        given()
                .when()
                .get("/aircraft/1").then()
                .contentType(ContentType.JSON)
                .statusCode(200);

//        String jsonActual = given()
//                .when()
//                .get("/aircraft/1").asString();
//
//        assertEquals(jsonExpected, jsonActual);
    }

    @Test
    public void findAircraftWithInvalidId() {
        int id = 5;

        given()
                .when()
                .get("/aircraft/" + id)
                .then()
                .log().body().and()
                .statusCode(404)
                .contentType(ContentType.JSON)
                .body("message", containsString("Entity not found! Id:5, Class:Aircraft."));
    }

}
