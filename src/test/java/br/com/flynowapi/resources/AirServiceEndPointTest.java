package br.com.flynowapi.resources;

import br.com.flynowapi.ApplicationTest;
import br.com.flynowapi.model.AirService;
import br.com.flynowapi.model.dto.AirServiceDTO;
import br.com.flynowapi.model.dto.AirServiceSimpleDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.http.ContentType;
import org.junit.Test;
import org.springframework.hateoas.Link;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class AirServiceEndPointTest extends ApplicationTest {


    @Test
    public void findAllServicesTest() throws JsonProcessingException {
        List<AirServiceSimpleDTO> dtoList = new ArrayList<>();
        Arrays.asList(serviceTravel, serviceCargo, serviceFreight).forEach(s -> {
                    var dto = new AirServiceSimpleDTO(s.getId(), s.getName(), s.getDescription(), s.getType().getDescription());
                    dtoList.add(dto);
                    s.getImages().forEach(l -> dto.add(Link.of(l, "images")));
                }
        );

        String jsonExpected = objectMapper.writeValueAsString(dtoList);
        String jsonActual = given().when().get("/services").asString();

        assertEquals(jsonExpected, jsonActual);
    }

    @Test
    public void findServiceByIdTest() throws JsonProcessingException {
        given()
                .when()
                .get("/services/1").then()
                .contentType(ContentType.JSON)
                .statusCode(200);
    }

    @Test
    public void findServicesByTypeTest() throws JsonProcessingException {
        List<AirServiceSimpleDTO> dtoList = new ArrayList<>();
        Arrays.asList(serviceTravel).forEach(s -> {
                    var dto = new AirServiceSimpleDTO(s.getId(), s.getName(), s.getDescription(), s.getType().getDescription());
                    dtoList.add(dto);
                    s.getImages().forEach(l -> dto.add(Link.of(l, "images")));
                }
        );

        String jsonExpected = objectMapper.writeValueAsString(dtoList);
        given()
                .when()
                .get("/services/type/travel").then()
                .contentType(ContentType.JSON)
                .statusCode(200);

        String jsonActual = given()
                .when()
                .get("/services/type/travel").asString();

        assertEquals(jsonExpected, jsonActual);
    }

    @Test
    public void findServicesByInvalidTypeTest() {
        String descriptionType = "something something";
        given()
                .when()
                .get("/services/type/" + descriptionType)
                .then()
                .log().body().and()
                .statusCode(400)
                .contentType(ContentType.JSON)
                .body("message", containsString("Invalid description <" + descriptionType + ">"));
    }

    @Test
    public void findAirServiceByInvalidIdTest() {
        int id = 5;

        given()
                .when()
                .get("/services/" + id)
                .then()
                .log().body().and()
                .statusCode(404)
                .contentType(ContentType.JSON)
                .body("message", containsString("Entity not found! Id:5, Class:AirService."));
    }

}
