package br.com.flynowapi.resources;

import br.com.flynowapi.ApplicationTest;
import br.com.flynowapi.model.Address;
import br.com.flynowapi.model.Company;
import br.com.flynowapi.model.dto.CompanySimpleDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.http.ContentType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;

public class CompanyEndPointTest extends ApplicationTest {

    @Test
    public void findListOfCompanies() throws JsonProcessingException {
        List<CompanySimpleDTO> dtoList = new ArrayList<>();
        Arrays.asList(company1, company2).forEach(c -> dtoList.add(new CompanySimpleDTO(c.getId(), c.getName())));

        String jsonExpected = objectMapper.writeValueAsString(dtoList);
        given()
                .when()
                .get("/companies").then()
                .contentType(ContentType.JSON)
                .statusCode(200);

        String jsonActual = given()
                .when()
                .get("/companies").asString();

        assertEquals(jsonExpected,jsonActual);
    }

    @Test
    public void findCompanyById() {
        int id = 1;

        given()
                .when()
                .get("/companies/" + id)
                .then()
                .log().body().and()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body("id", equalTo(id))
                .body("name", containsString("Taxi Aereo Santarem"));

    }

    @Test
    public void findCompanyWithInvalidId(){
        int id = 5;

        given()
                .when()
                .get("/companies/"+id)
                .then()
                .log().body().and()
                .statusCode(404)
                .contentType(ContentType.JSON)
                .body("message",containsString("Entity not found! Id:5, Class:Company."));
    }

}
