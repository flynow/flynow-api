package br.com.flynowapi;

import br.com.flynowapi.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class ApplicationTest {

    protected ObjectMapper objectMapper;

    protected Address address1;
    protected Address address2;
    protected Company company1;
    protected Company company2;
    protected Aircraft aircraft1;
    protected Aircraft aircraft2;
    protected AirService serviceTravel;
    protected AirService serviceCargo;
    protected AirService serviceFreight;

    @Value("${local.server.port}")
    protected int port;

    @Before
    public void setUp() throws ParseException {
        RestAssured.port = port;
        objectMapper = new ObjectMapper();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");


        address1 = new Address(1, "rua vinte e quatro de outubro", 2284, "sem complento", "fatima", "santarem", "para", "brasil", 68040010);
        address2 = new Address(2, "travessa frei ambrosio", 925, "casa b", "fatima", "santarem", "para", "brasil", 68040440);
        company1 = new Company(1, "Taxi Aereo Santarem", address1);
        company2 = new Company(2, "Taxi Aereo Voe Agora", address2);

        aircraft1 = new Aircraft(1, "CARAVAN C208", new Company(1, "Taxi Aereo Santarem", address1));
        aircraft1.addImage("https://fly-now-test.s3-sa-east-1.amazonaws.com/C208_1.jpg");
        aircraft1.addImage("https://fly-now-test.s3-sa-east-1.amazonaws.com/C208_2.JPG");
        aircraft2 = new Aircraft(2, "CESSNA 210", new Company(2, "Taxi Aereo Voe Agora", address2));
        aircraft2.addImage("https://fly-now-test.s3-sa-east-1.amazonaws.com/C210_1.jpg");
        aircraft2.addImage("https://fly-now-test.s3-sa-east-1.amazonaws.com/C210_2.jpg");


        serviceTravel = new Travel(1, "Viagem Santarem Belem", "description", "SBSN", "SBBE",
                true, sdf.parse("31/12/2020 23:59"), 7, company1, aircraft1,
                100.00, sdf.parse("1/12/2020 10:00"), 5, 30, 0);
        serviceTravel.addImage("https://fly-now-test.s3-sa-east-1.amazonaws.com/belem-01.jpg");

        serviceCargo = new Cargo(2, "Santarem - Belem", "description", "SBSN", "SBBE",
                true, sdf.parse("31/12/2020 23:59"), 7, company1, aircraft1,
                10.0, 1000.0, 50.0, 2.5, 5.0, 4.0);
        serviceCargo.addImage("https://fly-now-test.s3-sa-east-1.amazonaws.com/belem-02.jpg");

        serviceFreight = new Freight(3, "Frete de Aviao", "description", "SBSN", "SBBE",
                true, sdf.parse("31/12/2020 23:59"), 0, company2, aircraft2, 2000.00, 30);
        serviceFreight.addImage("https://fly-now-test.s3-sa-east-1.amazonaws.com/belem-03.jpg");
    }

}
